// Package d'ex�cution du programme
package run;

/* IMPORTS */
// Imports Java
// import java.util.ArrayList;

// Classes techniques
import common.Saisie;
import common.TextColor;

// Classes m�tier
import magasin.Article;
import magasin.Stock;

// Classe menu utilisateur
public class Menu {

    // M�thode menu protected pour l'acc�s depuis Debug (� privatiser en prod)
    protected static boolean menuGestion(Stock stockDeTravail) {
        // Menu utilisateur principal
        int choix = Saisie.lire_int(
                "[MENU PRINCIPAL] \n" +
                        "(1) Rechercher un article par r�f�rence \n" +
                        "(2) Rechercher un article par nom \n" +
                        "(3) Rechercher un article par intervalle de prix de vente \n" +
                        "(4) Ajouter un article au stock \n" +
                        "(5) Supprimer un article du stock \n" +
                        "(6) Modifier un article \n" +
                        "(7) Afficher tous les articles \n" +
                        "(0) Quitter");
        switch (choix) {
            case 0: {
                return false;
            }
            case 1: {
                // Rechercher un article par r�f�rence
                if (stockDeTravail.getCollectionArticles().isEmpty()) {
                    System.out.println(TextColor.RED + "Il n'y a aucun article � rechercher !" + TextColor.RESET);
                } else {
                    int ref = Saisie.lire_int("Quelle r�f�rence d'article recherchez-vous ?");
                    Article unArticle = stockDeTravail.rechercherArticle(ref);
                    if (unArticle == null) {
                        System.out.println("Aucun article n'a �t� trouv� avec cette r�f�rence !");
                    } else {
                        System.out.println(unArticle.toString());
                    }
                }
                return true;
            }
            case 2: {
                // Rechercher un article par nom
                if (stockDeTravail.getCollectionArticles().isEmpty()) {
                    System.out.println(TextColor.RED + "Il n'y a aucun article � rechercher !" + TextColor.RESET);
                } else {
                    String nom = Saisie.lire_String("Quel nom d'article recherchez-vous ?");
                    Article unArticle = stockDeTravail.rechercherArticle(nom);
                    if (unArticle == null) {
                        System.out.println("Aucun article n'a �t� trouv� avec ce nom !");
                    } else {
                        System.out.println(unArticle.toString());
                    }
                }
                return true;
            }
            case 3: {
                if (stockDeTravail.getCollectionArticles().isEmpty()) {
                    System.out.println(TextColor.RED + "Il n'y a aucun article � rechercher !" + TextColor.RESET);
                } else {
                    // Rechercher un article par intervalle de prix
                    double prixmin = Saisie.lire_double("Quel est le prix minimun ?");
                    double prixmax = Saisie.lire_double("Quel est le prix max ?");
                    Article unArticle = stockDeTravail.rechercherArticle(prixmin, prixmax);
                    if (unArticle == null) {
                        System.out.println("Aucun article n'a �t� trouv� avec cette r�f�rence !");
                    } else {
                        System.out.println(unArticle.toString());
                    }
                }
                return true;
            }
            case 4: {
                // Ajouter un article au stock
                Article unArticle = new Article();
                if (stockDeTravail.ajouterArticle(unArticle)) {
                    System.out.println("L'article a �t� ajout� avec succ�s !");
                } else {
                    System.out.println(
                            TextColor.RED
                                    + "Vous ne pouvez pas ajouter ce type d'article ! Modifiez sa quantit� depuis le menu"
                                    + TextColor.RESET);
                }
                return true;
            }
            case 5: {
                // Supprimer un article du stock
                if (stockDeTravail.getCollectionArticles().isEmpty()) {
                    System.out.println(TextColor.RED + "Il n'y a aucun article � supprimer !" + TextColor.RESET);
                } else {
                    int ref = Saisie.lire_int("Quelle article voulez-vous supprimer ? Donnez la r�f�rence.");
                    if (stockDeTravail.supprimerArticle(ref) == false) {
                        System.out.println("Impossible de supprimer cet article !");
                    } else {
                        System.out.println("Article supprim� correctement !");
                    }
                }
                return true;
            }
            case 6: {
                // Modifier un article
                if (stockDeTravail.getCollectionArticles().isEmpty()) {
                    System.out.println(TextColor.RED + "Il n'y a aucun article � rechercher !" + TextColor.RESET);
                } else {
                    int ref = Saisie.lire_int("Saisissez la r�f�rence de l'article que vous voulez modifier.");
                    if (stockDeTravail.modifierArticle(ref) == false) {
                        System.out.println("Article introuvable !");
                    } else {
                        System.out.println("Article modifi� correctement !");
                    }
                }
                return true;
            }
            case 7: {
                // Afficher tous les articles
                if (stockDeTravail.getCollectionArticles().isEmpty()) {
                    System.out.println(TextColor.RED + "Il n'y a aucun article � afficher !" + TextColor.RESET);
                } else {
                    System.out.println(stockDeTravail.toString());
                }
                return true;
            }
            default: {
                // Si choix inconnu
                System.out.println(TextColor.RED + "Ce choix n'existe pas !" + TextColor.RESET);
                return true;
            }
        }
    }

    // Point d'entr�e du programme
    public static void main(String[] args) {
        System.out.println(TextColor.PURPLE
                + "Ex�cution du programme utilisateur"
                +
                TextColor.RESET);

        // Cr�ation du stock sur lequel le menu travaille
        Stock unStock = new Stock();

        // Ajout de unStock � la liste des stocks
        // Menu.listeStocks.add(unStock);

        // On ex�cute le menu jusqu'� ce que l'utilisateur quitte et que Menu renvoie
        // false
        boolean runMenu = Menu.menuGestion(unStock);
        while (runMenu) {
            Saisie.lire_String("Appuyez sur Entr�e pour continuer...");
            runMenu = Menu.menuGestion(unStock);
        }
        System.out.println(
                TextColor.YELLOW
                        + "Merci d'avoir utilis� le programme de gestion de stocks !"
                        + TextColor.RESET);

    }
}