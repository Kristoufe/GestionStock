// Package d'ex�cution du programme
package run;

/* IMPORTS N�CESSAIRES AUX TESTS */
import magasin.Article;
import magasin.Stock;

/* CLASSE DE TESTS AUTOMATIS�S */
public class Debug {
    public static void main(String[] args) {

        /* CR�ATION DES INSTANCES DE TRAVAIL */
        // Cr�ation d'un stock
        Stock stockDArticles = new Stock();
        // Cr�ation d'un article
        Article nouvelArticle = new Article(01, "Truc", 15, 12);
        // Cr�ation d'un article avec la m�me r�f�rence
        // Article nouvelArticle2 = new Article(02, "Machin", 20, 12);

        /* TEST D'AJOUT D'ARTICLES */
        // On tente d'ajouter un article
        stockDArticles.ajouterArticle(nouvelArticle);
        // Si l'article a bien �t� ajout�
        // if (stockDArticles.ajouterArticle(nouvelArticle2) == true) {
        // System.out.println("Ajout� avec succ�s !");
        // } else {
        // // Sinon
        // System.out.println("Ajout impossible !");
        // }

        /* TESTS DE RECHERCHE D'ARTICLES */
        // Recherche par r�f�rence
        // System.out.println("Recherche 01:" +
        // stockDArticles.rechercherArticle(01).affiche());
        // Exemple d�taill� (variable � part)
        // Article articleARechercher = stockDArticles.rechercherArticle(01);
        // System.out.println("Recherche 123: \n" + articleARechercher.toString());

        // Recherche par nom

        // System.out.println("Recherche 'Truc':" +
        // stockDArticles.rechercherArticle("Truc").toString());

        // Recherche par intervalle de prix

        // System.out.println("Recherche 'Truc':" +
        // stockDArticles.rechercherArticle(0, 20).toString());

        /* TEST DE SUPPRESSION D'ARTICLE */

        // if (stockDArticles.supprimerArticle(nouvelArticle.getRef()) == true) {
        // System.out.println("Article supprim� !");
        // } else {
        // System.out.println("Article non supprim� !");
        // }

        /* TEST D'AFFICHAGE DU STOCK COMPLET */
        System.out.println(stockDArticles.toString());

        /* TEST AUTOMATIS� DU MENU */
        // Cr�ation d'un Stock
        boolean runMenu = Menu.menuGestion(stockDArticles);
        while (runMenu) {
            runMenu = Menu.menuGestion(stockDArticles);
        }
    }
}
