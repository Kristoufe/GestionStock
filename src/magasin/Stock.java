// Package des classes m�tier
package magasin;

/* IMPORTS */
// Imports java
import java.util.ArrayList;

public final class Stock {
    /* ATTRIBUTS PRIV�S */
    // Liste des articles de ce stock
    private ArrayList<Article> collectionArticles;

    /* CONSTRUCTEURS */
    // Constructeur programm� (pr�ciser la liste d'Articles du stock � instancier)
    public Stock(ArrayList<Article> collectionArticles) {
        this.collectionArticles = collectionArticles;
    }

    // Constructeur automatis� (nouvelle liste d'articles vierge)
    public Stock() {
        this.collectionArticles = new ArrayList<>();
    }

    /* toString */
    public String toString() { // = m�thode afficheTout()
        String stringToReturn = "";
        for (Article unArticle : this.getCollectionArticles()) {
            stringToReturn += unArticle.toString() + "\n";
        }
        return stringToReturn;
    }

    /* SETTERS */
    // D�finir la collection d'articles � utiliser pour ce type de stock
    public void setCollectionArticles(ArrayList<Article> collectionArticles) {
        this.collectionArticles = collectionArticles;
    }

    /* GETTERS */
    // R�cup�rer la collection d'articles � utiliser pour ce type de stock
    public ArrayList<Article> getCollectionArticles() {
        return collectionArticles;
    }

    /* AUTRES M�THODES */

    // Ajouter un article au stock
    public boolean ajouterArticle(Article article) {
        if (this.rechercherArticle(article.getRef()) == null) {
            // Si l'article n'existe pas d�j� en stock, l'ajouter
            this.getCollectionArticles().add(article);
            return true;
        } else {
            // Sinon, renvoyer un �chec
            return false;
        }
    }

    // Supprimer un article par r�f�rence
    public boolean supprimerArticle(int ref) {
        Article unArticle = this.rechercherArticle(ref);
        if (unArticle == null) {
            return false;
        }
        this.getCollectionArticles().remove(unArticle);
        return true;
    }

    // Modifier un article � partir de sa r�f�rence
    public boolean modifierArticle(int ref) {
        // R�cup�ration de l'article � partir de sa r�f�rence
        Article unArticle = this.rechercherArticle(ref);
        if (unArticle == null) {
            // Si l'article n'est pas en stock, renvoyer un �chec
            return false;
        } else {
            // Sinon, le modifier
            unArticle.modifier();
            return true;
        }
    }

    // Rechercher un article par r�f�rence (retourne null si aucun article trouv�)
    public Article rechercherArticle(int ref) {
        Article articleARetourner = null;
        for (Article unArticle : this.getCollectionArticles()) {
            if (unArticle.getRef() == ref) {
                articleARetourner = unArticle;
            }
        }
        return articleARetourner;
    }

    // Rechercher un article par nom (retourne null si aucun article trouv�)
    public Article rechercherArticle(String nom) {
        Article articleARetourner = null;
        for (Article unArticle : this.getCollectionArticles()) {
            // Note: .equals permet de v�rifier la valeur des String car ce sont des objets
            if (unArticle.getNom().equals(nom)) {
                articleARetourner = unArticle;
            }
        }
        return articleARetourner;
    }

    // Rechercher un article par r�f�rence (retourne null si aucun article trouv�)
    public Article rechercherArticle(double min, double max) {
        Article articleARetourner = null;
        for (Article unArticle : this.getCollectionArticles()) {
            if (unArticle.getPrix() >= min && unArticle.getPrix() <= max) {
                articleARetourner = unArticle;
            }
        }
        return articleARetourner;
    }
}