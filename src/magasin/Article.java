// Package des classes m�tier
package magasin;

// Import de la classe saisie utilisateur
import common.Saisie;
import common.TextColor;

public final class Article {
    /* ATTRIBUTS PRIV�S */
    // R�f�rence de l'article
    private int ref;
    // Nom de l'article
    private String nom;
    // Prix de l'article
    private int prix;
    // Quantit� de l'article en stock
    private int quantite;

    /* CONSTRUCTEURS */
    // Constructeur programm�
    public Article(int ref, String nom, int prix, int quantite) {
        this.ref = ref;
        this.nom = nom;
        this.prix = prix;
        this.quantite = quantite;
    }

    // Constructeur saisie utilisateur
    public Article() {
        this.ref = Saisie.lire_int("Quelle est la r�f�rence de l'article ?");
        this.nom = Saisie.lire_String("Quel est le nom de l'article ?");
        this.prix = Saisie.lire_int("� quel prix voulez-vous le vendre ?");
        this.quantite = Saisie.lire_int("Combien d'unit�s de cet article voulez-vous mettre en stock ? ?");
    }

    /* toString */
    public String toString() { // = m�thode affiche()
        return "Article N� " + ref + ": " + nom + "\n" +
                "(Prix: " + prix + " EUR)," + "\n" +
                "Quantit� en stock: " + quantite + "\n";
    }

    /* SETTERS */
    // D�finition de la r�f�rence de l'article
    public void setRef(int ref) {
        this.ref = ref;
    }

    // D�finition du nom de l'article
    public void setNom(String nom) {
        this.nom = nom;
    }

    // D�finition du prix de l'article
    public void setPrix(int prix) {
        this.prix = prix;
    }

    // D�finition de la quantit� de ce type d'articles en stock
    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    /* GETTERS */
    // R�cup�ration de la r�f�rence de l'article
    public int getRef() {
        return ref;
    }

    // R�cup�ration du nom de l'article
    public String getNom() {
        return this.nom;
    }

    // R�cup�ration du prix de l'article
    public int getPrix() {
        return this.prix;
    }

    // R�cup�ration du nombre de ce type d'articles en stock
    public int getQuantite() {
        return this.quantite;
    }

    // Modifier l'article
    public void modifier() {
        // Saisie utilisateur de la caract�ristique � changer
        String choix = Saisie.lire_String("Que voulez-vous changer ? (prix/nom/quantite)");
        choix = choix.toLowerCase();
        // Demander de saisir � nouveau tant que le choix est invalide
        while ((!choix.equals("prix")) && (!choix.equals("nom")) && (!choix.equals("quantite"))) {
            choix = Saisie.lire_String("Que voulez-vous changer ? (prix/nom/quantite)");
            choix = choix.toLowerCase();
        }

        // Traitement du choix
        if (choix.equals("prix")) {
            int newPrix = Saisie.lire_int(("Quel est le nouveau prix de l'article ?"));
            while (newPrix < 0) {
                System.out.println(TextColor.RED + "Vous ne pouvez pas d�finir un prix n�gatif !" + TextColor.RESET);
                newPrix = Saisie.lire_int(("Quel est le nouveau prix de l'article ?"));
            }
            this.setPrix(newPrix);
        } else if (choix.equals("nom")) {
            String newNom = Saisie.lire_String("Quel est le nouveau nom de l'article ?");
            this.setNom(newNom);
        } else if (choix.equals("quantite")) {
            int newQuantite = Saisie.lire_int(("Combien d'unit�s de cet article voulez-vous en stock ?"));
            while (newQuantite < 0) {
                System.out.println(TextColor.RED + "Vous ne pouvez pas d�finir un montant n�gatif !" + TextColor.RESET);
                newQuantite = Saisie.lire_int(("Combien d'unit�s de cet article voulez-vous en stock ?"));
            }
            this.setQuantite(newQuantite);
        }
    }
}